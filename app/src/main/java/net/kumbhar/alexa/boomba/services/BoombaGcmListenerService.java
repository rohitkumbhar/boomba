/*
 * Copyright 2015 Rohit Kumbhar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kumbhar.alexa.boomba.services;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import com.google.android.gms.gcm.GcmListenerService;
import net.kumbhar.alexa.boomba.BoombaLaunch;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class BoombaGcmListenerService extends GcmListenerService {
    private static final String TAG = "BoombaGcmListener";

    public static final Integer[] START_CLEANING = new Integer[]{135};
    public static final Integer[] RETURN_TO_DOCK = new Integer[]{143};
    public static final Integer[] SLEEP = new Integer[]{133};
    public static final Integer[] BEEP = new Integer[]{140, 1, 6, 55, 32, 95, 32, 55, 32, 95, 32, 55, 32, 111, 32, 141, 1, 131};

    public static final Map<String, Integer[]> COMMAND_SEQUENCES = new HashMap<>();

    static {
        COMMAND_SEQUENCES.put("beep", BEEP);
        COMMAND_SEQUENCES.put("clean", START_CLEANING);
        COMMAND_SEQUENCES.put("sleep", SLEEP);
        COMMAND_SEQUENCES.put("dock", RETURN_TO_DOCK);
    }

    @Override
    public void onMessageReceived(String from, Bundle data) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String command = data.getString("roomba_command");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Command: " + command);

        Integer[] commandSequence = COMMAND_SEQUENCES.get(command);

        if (commandSequence != null) {

            final BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            final String selectedRoombaDeviceName = sharedPreferences.getString(BoombaLaunch.ROOMBA_BLUETOOTH_ID, "");
            final Set<BluetoothDevice> bondedDevices = bluetoothAdapter.getBondedDevices();

            BluetoothDevice device = null;
            for (BluetoothDevice bondedDevice : bondedDevices) {
                if (bondedDevice.getName().equals(selectedRoombaDeviceName)) {
                    device = bondedDevice;
                    break;
                }
            }

            if (device != null && device.getUuids() != null && device.getUuids().length > 0) {

                bluetoothAdapter.cancelDiscovery();

                final UUID uuid = device.getUuids()[0].getUuid();
                BluetoothSocket socket = null;
                try {
                    socket = device.createRfcommSocketToServiceRecord(uuid);
                    while (!socket.isConnected()) {
                        socket.connect();
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                    }


                    OutputStream os = socket.getOutputStream();
                    initialize(os);

                    for (Integer opcode : commandSequence) {
                        send(os, opcode);
                    }

                    os.close();

                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (socket != null) {
                        try {
                            socket.close();
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
            }
        } else {
            Log.w(TAG, "No command sequence found for command: " + command);
        }


    }

    private void initialize(OutputStream os) throws IOException {

        send(os, 128);
        send(os, 129);
        send(os, 10);

        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            // Ignore
        }

        send(os, 130);
        send(os, 131);
    }

    private void send(OutputStream os, Integer opcode) throws IOException {
        os.write(opcode.byteValue());
        os.flush();
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            // Ignore
        }

    }

}
