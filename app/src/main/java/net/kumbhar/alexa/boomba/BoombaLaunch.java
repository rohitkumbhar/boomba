/*
 * Copyright 2015 Rohit Kumbhar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kumbhar.alexa.boomba;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.*;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import net.kumbhar.alexa.boomba.services.RegistrationIntentService;

import java.util.Set;

import static net.kumbhar.alexa.boomba.services.RegistrationIntentService.ECHO_PAIRING_CODE;
import static net.kumbhar.alexa.boomba.services.RegistrationIntentService.REGISTRATION_FAILED;


public class BoombaLaunch extends AppCompatActivity {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "BoombaLaunch";
    public static final String ROOMBA_BLUETOOTH_ID = "ROOMBA_BLUETOOTH_ID";
    public static final int REQUEST_ENABLE_BT = 5555;

    private BroadcastReceiver registrationBroadcastReceiver;
    private ProgressBar registrationProgressBar;
    private TextView informationTextView;
    private EditText pairingCodeTextView;
    private EditText bluetoothDeviceIdView;
    private Button selectRoombaButton;
    private TextView currentlyPairedDevicesView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boomba_launch);

        registrationProgressBar = (ProgressBar) findViewById(R.id.registrationProgressBar);
        registrationBroadcastReceiver = new RegistrationBroadcastReceiver();
        informationTextView = (TextView) findViewById(R.id.informationTextView);
        pairingCodeTextView = (EditText) findViewById(R.id.pairingCode);
        bluetoothDeviceIdView = (EditText) findViewById(R.id.pairedBlueetoothDevice);
        currentlyPairedDevicesView = (TextView) findViewById(R.id.currentlyPairedDevices);

        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(BoombaLaunch.this);
        final String currentPairingCode = sharedPreferences.getString(ECHO_PAIRING_CODE, "");
        final String currentRoombaBluetoothId = sharedPreferences.getString(ROOMBA_BLUETOOTH_ID, "");

        pairingCodeTextView.setText(currentPairingCode);

        // We may have launched off a click on the Echo app
        final String updatedPairingCode = getPairingCodeFromLaunchData();

        // Check ability to take GCM messages
        if (checkPlayServices()) {
            final Button pairButton = (Button) findViewById(R.id.pairButton);
            pairButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final String pairingCode = pairingCodeTextView.getText().toString();

                    // Only trigger intent if the pairing code has changed
                    if (currentPairingCode == null || !currentPairingCode.equals(pairingCode)) {
                        sharedPreferences.edit().putString(ECHO_PAIRING_CODE, pairingCode).apply();
                        Intent intent = new Intent(BoombaLaunch.this, RegistrationIntentService.class);
                        startService(intent);
                    }
                }
            });


            if ((currentPairingCode == null || currentPairingCode.isEmpty())
                    && updatedPairingCode != null
                    && !updatedPairingCode.isEmpty()
                    && !updatedPairingCode.equals(currentPairingCode)) {

                // Start IntentService to register this application with GCM.
                Intent intent = new Intent(this, RegistrationIntentService.class);
                startService(intent);
            } else {
                registrationProgressBar.setVisibility(ProgressBar.GONE);

                if (currentPairingCode == null || currentPairingCode.isEmpty()) {
                    informationTextView.setText("Enter your pairing code from the Echo app.");
                } else {
                    informationTextView.setText("You seem to be paired alright. Do you have a new code?");
                }
            }
        } else {
            informationTextView.setText("Device does not support Google Play Services. This application will not work.");
            informationTextView.setTextColor(Color.RED);
            return;
        }

        selectRoombaButton = (Button) findViewById(R.id.selectRoombaButton);
        final BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (bluetoothAdapter == null) {
            informationTextView.setText("Device does not support Bluetooth. This application will not work.");
            informationTextView.setTextColor(Color.RED);
            selectRoombaButton.setEnabled(false);
            return;
        }

        if (!bluetoothAdapter.isEnabled() && (currentRoombaBluetoothId == null || currentRoombaBluetoothId.isEmpty())) {
            selectRoombaButton.setText("Enable Bluetooth");
        } else {
            setupBluetoothDeviceSelection(currentRoombaBluetoothId, bluetoothAdapter);
            selectRoombaButton.setText("Save selected device");
        }

        selectRoombaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!bluetoothAdapter.isEnabled()) {
                    // Ask to enable bluetooth
                    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                } else {
                    String selectedDeviceName = bluetoothDeviceIdView.getText().toString();
                    sharedPreferences.edit().putString(ROOMBA_BLUETOOTH_ID, selectedDeviceName).apply();
                    currentlyPairedDevicesView.setText("Saved " + selectedDeviceName + " as roomba");
                }
            }
        });


    }

    private void setupBluetoothDeviceSelection(String currentRoombaBluetoothId, BluetoothAdapter bluetoothAdapter) {


        Set<BluetoothDevice> bondedDevices = bluetoothAdapter.getBondedDevices();
        StringBuilder sb = new StringBuilder();
        int count = 0;

        for (BluetoothDevice bondedDevice : bondedDevices) {
            sb.append(bondedDevice.getName()).append(",");
            if (bondedDevice.getName().equals(currentRoombaBluetoothId)) {
                bluetoothDeviceIdView.setText(currentRoombaBluetoothId);
            }
            count++;
        }

        String s = currentlyPairedDevicesView.getText().toString();
        String updated = s + sb.toString();
        currentlyPairedDevicesView.setText(updated);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_ENABLE_BT && resultCode == RESULT_OK) {
            final BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(BoombaLaunch.this);
            final String currentRoombaBluetoothId = sharedPreferences.getString(ROOMBA_BLUETOOTH_ID, "");
            setupBluetoothDeviceSelection(currentRoombaBluetoothId, bluetoothAdapter);
            selectRoombaButton.setText("Save selected device");
        }
    }

    /**
     * Get the pairing code from the custom intent scheme
     *
     * @return Pairing code
     */
    private String getPairingCodeFromLaunchData() {

        final Uri data = getIntent().getData();
        if (data == null || data.getPath() == null || data.getPath().isEmpty()) {
            return "";
        }

        final String path = data.getPath();
        int index = path.lastIndexOf("/");

        if (index == -1) {
            return path;
        } else {
            return path.substring(index + 1);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(registrationBroadcastReceiver,
                new IntentFilter(RegistrationIntentService.REGISTRATION_COMPLETE));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(registrationBroadcastReceiver);
        super.onPause();
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    private class RegistrationBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            final String action = intent.getAction();
            registrationProgressBar.setVisibility(ProgressBar.GONE);
            switch (action) {
                case RegistrationIntentService.REGISTRATION_COMPLETE:
                    informationTextView.setText(getString(R.string.gcm_pair_success));
                    break;
                case REGISTRATION_FAILED:
                    informationTextView.setText(getString(R.string.gcm_pair_fail));
                    break;
            }
        }
    }
}
