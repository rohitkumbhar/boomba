/*
 * Copyright 2015 Rohit Kumbhar
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.kumbhar.alexa.boomba.services;


import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import net.kumbhar.alexa.boomba.R;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;
import java.io.*;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

public class RegistrationIntentService extends IntentService {


    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String REGISTRATION_FAILED = "registrationFailed";

    private static final String TAG = "RegIntentService";
    private static final String[] TOPICS = {"global"};
    public static final String ECHO_PAIRING_CODE = "echo_pairing_code";
    public static final String UTF_8 = "UTF-8";


    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        try {
            synchronized (TAG) {

                InstanceID instanceID = InstanceID.getInstance(this);
                String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

                Log.i(TAG, "GCM Registration Token: " + token);

                sendRegistrationToServer(token, sharedPreferences);
                sharedPreferences.edit().putBoolean(SENT_TOKEN_TO_SERVER, false).apply();

                Intent registrationComplete = new Intent(REGISTRATION_COMPLETE);
                LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);

            }
        } catch (Exception e) {
            Log.d(TAG, "Failed to complete token refresh", e);
            Intent registrationComplete = new Intent(REGISTRATION_FAILED);
            LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
        }

    }

    /**
     * Persist registration to third-party servers.
     * <p/>
     * Modify this method to associate the user's GCM registration token with any server-side account
     * maintained by your application.
     *
     * @param token             The new token.
     * @param sharedPreferences Application shared preferences
     */
    private void sendRegistrationToServer(String token, SharedPreferences sharedPreferences) throws IOException, CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {

        // Check if we have the Boomba skill registration key
        final String pairingCode = sharedPreferences.getString(ECHO_PAIRING_CODE, "");

        if (pairingCode != null && !pairingCode.isEmpty()) {

            // Call the server with the pairing code
            final SSLSocketFactory socketFactory = getSslSocketFactory();
            final URL url = new URL("https://alexa.kumbhar.net/registration/");
            final HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setSSLSocketFactory(socketFactory);
            conn.setDoInput(true);
            conn.setDoOutput(true);

            final List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("registration_key", pairingCode));
            params.add(new BasicNameValuePair("gcm_id", token));

            final OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, UTF_8));
            writer.write(getQuery(params));
            writer.flush();
            writer.close();
            os.close();

            final InputStream in = conn.getInputStream();
            byte[] buff = new byte[1024];
            int len = in.read(buff);

            final StringBuilder sb = new StringBuilder();
            while (len != -1) {
                sb.append(new String(buff, 0, len));
                len = in.read(buff);
            }
            Log.i(TAG, sb.toString());
        }


    }

    /**
     * Get SSL SocketFactory for a self-signed certificate
     *
     * @return SSL socket factory
     * @throws CertificateException
     * @throws IOException
     * @throws KeyStoreException
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     */
    private SSLSocketFactory getSslSocketFactory() throws CertificateException, IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        Certificate ca;

        try (InputStream caInput = this.getResources().openRawResource((R.raw.server_certificate))) {
            ca = cf.generateCertificate(caInput);
            Log.i(TAG, "ca=" + ((X509Certificate) ca).getSubjectDN());
        }

        // Create a KeyStore containing our trusted CAs
        String keyStoreType = KeyStore.getDefaultType();
        KeyStore keyStore = KeyStore.getInstance(keyStoreType);
        keyStore.load(null, null);
        keyStore.setCertificateEntry("ca", ca);

        // Create a TrustManager that trusts the CAs in our KeyStore
        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
        tmf.init(keyStore);

        // Create an SSLContext that uses our TrustManager
        SSLContext context = SSLContext.getInstance("TLS");
        context.init(null, tmf.getTrustManagers(), null);
        return context.getSocketFactory();
    }

    /**
     * Construct query params for the list of name-value pairs
     *
     * @param params List of name value pairs
     * @return
     * @throws UnsupportedEncodingException
     */
    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params) {
            if (first) {
                first = false;
            } else {
                result.append("&");
            }

            result.append(URLEncoder.encode(pair.getName(), UTF_8));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), UTF_8));
        }

        return result.toString();
    }

    /**
     * Subscribe to any GCM topics of interest, as defined by the TOPICS constant.
     *
     * @param token GCM token
     * @throws IOException if unable to reach the GCM PubSub service
     */
    // [START subscribe_topics]
    private void subscribeTopics(String token) throws IOException {
        for (String topic : TOPICS) {
            GcmPubSub pubSub = GcmPubSub.getInstance(this);
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }
}
